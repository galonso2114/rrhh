﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RRHH.Models
{
    public class Schoolsandhub
    {
        public ulong Id { get; set; }
        public ulong? SchoolNumbers { get; set; }
        public ulong? SchoolHubs { get; set; }
        public DateTime? Date { get; set; }
        public ulong? iduser { get; set; }
    }
}
