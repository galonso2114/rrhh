﻿using System;
using System.Collections.Generic;

#nullable disable

namespace RRHH.Models
{
    public partial class Project
    {
        public ulong Idproject { get; set; }
        public string Name { get; set; }
    }
}
