﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RRHH.Models.VM
{
    public class HomeVM
    {
        public HomeVM()
        {
            consolidatedHours = new List<ConsolidatedHoursTable>();
        }
        public List<ConsolidatedHoursTable> consolidatedHours { get; set; }
        public int CountTotalEmployee { get; set; }
        public int CountEmployeeByProject { get; set; }
        public ulong idpro { get; set; }
        public DateTime week { get; set; }
        public decimal totalfin { get; set; }
    }
}
