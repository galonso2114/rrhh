﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RRHH.Models.VM
{
    public class ConsolidatedHoursTable
    {
        public string name { get; set; }
        public string lasname { get; set; }
        public decimal hours { get; set; }
        public decimal Payrate { get; set; }
        public decimal payroll { get; set; }

    }
}
