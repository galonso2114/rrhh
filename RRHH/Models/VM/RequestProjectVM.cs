﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RRHH.Models.VM
{
    public class RequestProjectVM
    {
        public ulong id { get; set; }
        public ulong idusuario { get; set; }
        public ulong idprojecto { get; set; }
    }
}
