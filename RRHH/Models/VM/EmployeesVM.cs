﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RRHH.Models.VM
{
    public class EmployeesVM
    {
        public ulong Id { get; set; }
        public ulong? Idproject { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public decimal? Payrate { get; set; }
        public DateTime? Startdate { get; set; }
        public int? State { get; set; }
        public string project { get; set; }
        public decimal horas { get; set; }
    }
}
