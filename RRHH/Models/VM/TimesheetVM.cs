﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RRHH.Models.VM
{
    public class TimesheetVM
    {
        public TimesheetVM()
        {
            timesheets = new List<Timesheet>();
        }
        public int? IdEmployee { get; set; }
        public string EmployeeName { get; set; }
        public ulong Idtimesheet { get; set; }
        public string Description { get; set; }
        public DateTime? Date { get; set; }
        public TimeSpan? Start { get; set; }
        public TimeSpan? End { get; set; }
        public float? Lunch { get; set; }
        public ulong? Idproject { get; set; }
        public List<Timesheet> timesheets { get; set; }
    }
}
