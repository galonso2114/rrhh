﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RRHH.Models.VM
{
    public class UserVM
    {

        public ulong Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public bool? Active { get; set; }
        public ulong? Idrole { get; set; }
        public ulong? Idproject { get; set; }
        public string role { get; set; }
        public string Project { get; set; }

    }
}
