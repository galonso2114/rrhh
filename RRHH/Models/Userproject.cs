﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RRHH.Models
{
    public partial class Userproject
    {
        public ulong Id { get; set; }
        public ulong? Iduser { get; set; }
        public ulong? Idproject { get; set; }
        public bool? Active { get; set; }
    }
}
