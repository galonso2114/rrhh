﻿using System;
using System.Collections.Generic;

#nullable disable

namespace RRHH.Models
{
    public partial class Role
    {
        public ulong Idrole { get; set; }
        public string Name { get; set; }
    }
}
