﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace RRHH.Models
{
    public partial class rrhhContext : DbContext
    {
        public rrhhContext()
        {
        }

        public rrhhContext(DbContextOptions<rrhhContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Document> Documents { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Expense> Expenses { get; set; }
        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Schoolsandhub> Schoolsandhubs { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<Stateapruval> Stateapruvals { get; set; }
        public virtual DbSet<Timesheet> Timesheets { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Userproject> Userprojects { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySql("server=45.34.15.180;user=root;password=N0v$t10n;database=rrhh", Microsoft.EntityFrameworkCore.ServerVersion.Parse("10.5.9-mariadb"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            modelBuilder.Entity<Document>(entity =>
            {
                entity.HasKey(e => e.IdDocument)
                    .HasName("PRIMARY");

                entity.ToTable("document");

                entity.Property(e => e.IdDocument)
                    .HasColumnType("bigint(20) unsigned")
                    .HasColumnName("idDocument");

                entity.Property(e => e.IdEmployee)
                    .HasColumnType("bigint(20) unsigned")
                    .HasColumnName("idEmployee");

                entity.Property(e => e.Name)
                    .HasMaxLength(300)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.ToTable("employees");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(20) unsigned")
                    .HasColumnName("id");

                entity.Property(e => e.Address)
                    .HasMaxLength(150)
                    .HasColumnName("address");

                entity.Property(e => e.Email)
                    .HasMaxLength(150)
                    .HasColumnName("email");

                entity.Property(e => e.Idproject)
                    .HasColumnType("int(11)")
                    .HasColumnName("idproject");

                entity.Property(e => e.LastName)
                    .HasMaxLength(150)
                    .HasColumnName("lastName");

                entity.Property(e => e.Name)
                    .HasMaxLength(150)
                    .HasColumnName("name");

                entity.Property(e => e.Payrate)
                    .HasPrecision(10)
                    .HasColumnName("payrate");

                entity.Property(e => e.Startdate)
                    .HasColumnType("datetime")
                    .HasColumnName("startdate");

                entity.Property(e => e.State)
                    .HasColumnType("int(11)")
                    .HasColumnName("state");

                entity.Property(e => e.Telephone)
                    .HasMaxLength(150)
                    .HasColumnName("telephone");
            });

            modelBuilder.Entity<Expense>(entity =>
            {
                entity.ToTable("expenses");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(20) unsigned")
                    .HasColumnName("id");

                entity.Property(e => e.Date)
                    .HasColumnType("datetime")
                    .HasColumnName("date");

                entity.Property(e => e.Deliverdate)
                    .HasColumnType("datetime")
                    .HasColumnName("deliverdate");

                entity.Property(e => e.Description)
                    .HasMaxLength(20)
                    .HasColumnName("description");

                entity.Property(e => e.Idemployee)
                    .HasColumnType("bigint(20) unsigned")
                    .HasColumnName("idemployee");

                entity.Property(e => e.Idproject)
                    .HasColumnType("bigint(20) unsigned")
                    .HasColumnName("idproject");

                entity.Property(e => e.Payday)
                    .HasColumnType("datetime")
                    .HasColumnName("payday");

                entity.Property(e => e.State)
                    .HasColumnType("bigint(20) unsigned")
                    .HasColumnName("state");

                entity.Property(e => e.Value).HasColumnName("value");
            });

            modelBuilder.Entity<Project>(entity =>
            {
                entity.HasKey(e => e.Idproject)
                    .HasName("PRIMARY");

                entity.ToTable("project");

                entity.Property(e => e.Idproject)
                    .HasColumnType("bigint(20) unsigned")
                    .HasColumnName("idproject");

                entity.Property(e => e.Name)
                    .HasMaxLength(150)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.HasKey(e => e.Idrole)
                    .HasName("PRIMARY");

                entity.ToTable("role");

                entity.Property(e => e.Idrole)
                    .HasColumnType("bigint(20) unsigned")
                    .HasColumnName("idrole");

                entity.Property(e => e.Name)
                    .HasMaxLength(150)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<Schoolsandhub>(entity =>
            {
                entity.ToTable("schoolsandhubs");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(20) unsigned")
                    .HasColumnName("id");

                entity.Property(e => e.Date)
                    .HasColumnType("datetime")
                    .HasColumnName("date");

                entity.Property(e => e.SchoolHubs)
                    .HasColumnType("bigint(20) unsigned")
                    .HasColumnName("schoolHubs");

                entity.Property(e => e.SchoolNumbers)
                    .HasColumnType("bigint(20) unsigned")
                    .HasColumnName("schoolNumbers");
            });

            modelBuilder.Entity<State>(entity =>
            {
                entity.HasKey(e => e.IdState)
                    .HasName("PRIMARY");

                entity.ToTable("state");

                entity.Property(e => e.IdState)
                    .HasColumnType("bigint(20) unsigned")
                    .HasColumnName("idState");

                entity.Property(e => e.Name)
                    .HasMaxLength(300)
                    .HasColumnName("name");
            });
            modelBuilder.Entity<Stateapruval>(entity =>
            {
                entity.ToTable("stateapruvals");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(20) unsigned")
                    .HasColumnName("id");

                entity.Property(e => e.Active).HasColumnName("active");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .HasColumnName("name");
            });
            modelBuilder.Entity<Timesheet>(entity =>
            {
                entity.HasKey(e => e.Idtimesheet)
                    .HasName("PRIMARY");

                entity.ToTable("timesheet");

                entity.Property(e => e.Idtimesheet)
                    .HasColumnType("bigint(20) unsigned")
                    .HasColumnName("idtimesheet");

                entity.Property(e => e.Date)
                    .HasColumnType("datetime")
                    .HasColumnName("date");

                entity.Property(e => e.Description)
                    .HasMaxLength(300)
                    .HasColumnName("description");

                entity.Property(e => e.End)
                    .HasColumnType("time")
                    .HasColumnName("end");

                entity.Property(e => e.IdEmployee)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("idEmployee");

                entity.Property(e => e.Idproject).HasColumnType("bigint(20)");

                entity.Property(e => e.Lunch).HasColumnName("lunch");

                entity.Property(e => e.Start)
                    .HasColumnType("time")
                    .HasColumnName("start");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(20) unsigned")
                    .HasColumnName("id");

                entity.Property(e => e.Active).HasColumnName("active");

                entity.Property(e => e.Idproject)
                    .HasColumnType("bigint(20)")
                    .HasColumnName("idproject");

                entity.Property(e => e.Idrole)
                    .HasColumnType("int(11)")
                    .HasColumnName("idrole");

                entity.Property(e => e.Name)
                    .HasMaxLength(150)
                    .HasColumnName("name");

                entity.Property(e => e.Password)
                    .HasMaxLength(150)
                    .HasColumnName("password");
            });

            modelBuilder.Entity<Userproject>(entity =>
            {
                entity.ToTable("userproject");

                entity.Property(e => e.Id)
                    .HasColumnType("bigint(20) unsigned")
                    .HasColumnName("id");

                entity.Property(e => e.Active).HasColumnName("active");

                entity.Property(e => e.Idproject)
                    .HasColumnType("bigint(20) unsigned")
                    .HasColumnName("idproject");

                entity.Property(e => e.Iduser)
                    .HasColumnType("bigint(20) unsigned")
                    .HasColumnName("iduser");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
