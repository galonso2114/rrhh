﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RRHH.Models
{
    public class Expense
    {
        public ulong Id { get; set; }
        public DateTime? Date { get; set; }
        public string Description { get; set; }
        public ulong? Idproject { get; set; }
        public ulong? Idemployee { get; set; }
        public double? Value { get; set; }
        public ulong? State { get; set; }
        public DateTime? Deliverdate { get; set; }
        public DateTime? Payday { get; set; }
    }
}
