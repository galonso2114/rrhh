﻿using System;
using System.Collections.Generic;

#nullable disable

namespace RRHH.Models
{
    public partial class Document
    {
        public ulong IdDocument { get; set; }
        public ulong? IdEmployee { get; set; }
        public string Name { get; set; }
    }
}
