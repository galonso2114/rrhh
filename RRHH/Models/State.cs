﻿using System;
using System.Collections.Generic;

#nullable disable

namespace RRHH.Models
{
    public partial class State
    {
        public ulong IdState { get; set; }
        public string Name { get; set; }
    }
}
