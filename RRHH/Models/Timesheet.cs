﻿using System;
using System.Collections.Generic;

#nullable disable

namespace RRHH.Models
{
    public partial class Timesheet
    {
        public ulong Idtimesheet { get; set; }
        public string Description { get; set; }
        public DateTime? Date { get; set; }
        public TimeSpan? Start { get; set; }
        public TimeSpan? End { get; set; }
        public ulong? IdEmployee { get; set; }
        public float? Lunch { get; set; }
        public ulong? Idproject { get; set; }
    }
}
