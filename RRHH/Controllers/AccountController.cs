﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RRHH.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RRHH.Controllers
{
    public class AccountController : Controller
    {
        private readonly rrhhContext _context;
        public AccountController(rrhhContext context)
        {
            _context = context;
        }
        // GET: AccountController
        public ActionResult Login()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {

                throw;
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login([Bind("Name,Password")] User user)
        {
            try
            {
                var usuario = _context.Users.FirstOrDefault(x => x.Name.Equals(user.Name));
                if (usuario != null)
                {
                    if (usuario.Password.Equals(user.Password))
                    {
                        var str = JsonConvert.SerializeObject(usuario);
                        HttpContext.Session.SetString("User", str);
                        return RedirectToAction(nameof(Index),"Home");
                    }
                    else
                    {
                        return RedirectToAction(nameof(Login));
                    }
                }
                return RedirectToAction(nameof(Login));
            }
            catch (Exception)
            {

                throw;
            }
        }
        public IActionResult logout()
        {
            try
            {
                var str = HttpContext.Session.GetString("User");
                var SessionUsuario = JsonConvert.DeserializeObject<User>(str);
                HttpContext.Session.Clear();
                return RedirectToAction("Login", "Account");
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
