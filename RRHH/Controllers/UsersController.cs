﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RRHH.Filters;
using RRHH.Models;
using RRHH.Models.VM;

namespace RRHH.Controllers
{
    [CustomAuthorize]
    public class UsersController : Controller
    {
        private readonly rrhhContext _context;

        public UsersController(rrhhContext context)
        {
            _context = context;
        }

        // GET: Users
        public async Task<IActionResult> Index()
        {
            List<UserVM> users = new List<UserVM>();
            var user = await _context.Users.ToListAsync();
            foreach (var item in user)
            {
                UserVM vM = new UserVM();
                vM.Active = item.Active;
                vM.Id = item.Id;
                vM.Idproject = item.Idproject;
                vM.Idrole = item.Idrole;
                vM.Name = item.Name;
                vM.Password = item.Password;
                vM.Project = _context.Projects.Find(item.Idproject) != null ? _context.Projects.Find(item.Idproject).Name : "";
                vM.role = _context.Roles.Find(item.Idrole).Name;
                users.Add(vM);
            }
            return View(users);
        }

        // GET: Users/Details/5
        public async Task<IActionResult> Details(ulong? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // GET: Users/Create
        public IActionResult Create()
        {
            var listrol = (from rol in _context.Roles
                           select new SelectListItem()
                           {
                               Text = rol.Name,
                               Value = rol.Idrole.ToString(),
                           }).ToList();
            ViewBag.roles = listrol;
            var listpro = (from rol in _context.Projects
                           select new SelectListItem()
                           {
                               Text = rol.Name,
                               Value = rol.Idproject.ToString(),
                           }).ToList();
            ViewBag.project = listpro;
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Password,Active,Idrole,Idproject")] User user)
        {
            if (ModelState.IsValid)
            {
                _context.Add(user);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(user);
        }

        // GET: Users/Edit/5
        public async Task<IActionResult> Edit(ulong? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            var listrol = (from rol in _context.Roles
                           select new SelectListItem()
                           {
                               Text = rol.Name,
                               Value = rol.Idrole.ToString(),
                           }).ToList();
            ViewBag.roles = listrol;
            var listpro = (from rol in _context.Projects
                           select new SelectListItem()
                           {
                               Text = rol.Name,
                               Value = rol.Idproject.ToString(),
                           }).ToList();
            ViewBag.project = listpro;
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ulong id, [Bind("Id,Name,Password,Active,Idrole,Idproject")] User user)
        {
            if (id != user.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(user);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserExists(user.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public async Task<IActionResult> Delete(ulong? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(ulong id)
        {
            var user = await _context.Users.FindAsync(id);
            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UserExists(ulong id)
        {
            return _context.Users.Any(e => e.Id == id);
        }
        [HttpPost]
        public async Task<IActionResult> AddProject(RequestProjectVM vM)
        {
            try
            {
                var userpro = await _context.Userprojects.FirstOrDefaultAsync(x => x.Idproject == vM.idprojecto && x.Iduser == vM.idusuario);
                if (userpro == null)
                {
                    Userproject userproject = new Userproject();
                    userproject.Idproject = vM.idprojecto;
                    userproject.Iduser = vM.idusuario;
                    userproject.Active = true;
                    _context.Set<Userproject>().Add(userproject);
                    _context.SaveChanges();
                }
                return RedirectToAction(nameof(Index));
            }
            catch (Exception)
            {

                throw;
            }
        }
        [HttpGet]
        public async Task<IActionResult> getProject()
        {
            try
            {
                var project = await _context.Projects.ToListAsync();
                return Json(project);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
