﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RRHH.Filters;
using RRHH.Models;
using RRHH.Models.VM;

namespace RRHH.Controllers
{
    [CustomAuthorize]
    public class EmployeesController : Controller
    {
        private readonly rrhhContext _context;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public EmployeesController(rrhhContext context, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _webHostEnvironment = webHostEnvironment;
        }

        // GET: Employees
        public async Task<IActionResult> Index()
        {
            try
            {
                List<EmployeesVM> employees = new List<EmployeesVM>();
                var str = HttpContext.Session.GetString("User");
                var SessionUsuario = JsonConvert.DeserializeObject<User>(str);
                if (SessionUsuario.Idrole == 1)
                {
                    var empleados = await _context.Employees.ToListAsync();
                    foreach (var item in empleados)
                    {
                        EmployeesVM vM = new EmployeesVM();
                        vM.Address = item.Address;
                        vM.Email = item.Email;
                        vM.Id = item.Id;
                        vM.Idproject = item.Idproject;
                        vM.LastName = item.LastName;
                        vM.Name = item.Name;
                        vM.Payrate = item.Payrate;
                        vM.project = _context.Projects.Find(item.Idproject).Name;
                        vM.Startdate = item.Startdate;
                        vM.State = item.State;
                        vM.Telephone = item.Telephone;
                        employees.Add(vM);
                    }
                    return View(employees);
                }
                else
                {
                    var empleados = await _context.Employees.Where(x => x.Idproject == SessionUsuario.Idproject).ToListAsync();
                    return View(empleados);
                }
                
            }
            catch (Exception ex)
            {
                var msn = ex.Message;
                throw;
            }

        }

        // GET: Employees/Details/5
        public async Task<IActionResult> Details(ulong? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees
                .FirstOrDefaultAsync(m => m.Id == id);
            if (employee == null)
            {
                return NotFound();
            }
            var document = _context.Documents.Where(x => x.IdEmployee == employee.Id).ToList();
            EmployeeDetails details = new EmployeeDetails();
            details.Address = employee.Address;
            details.documents = document;
            details.Email = employee.Email;
            details.Id = employee.Id;
            details.Idproject = employee.Idproject;
            details.LastName = employee.LastName;
            details.Name = employee.Name;
            details.Payrate = employee.Payrate;
            details.Startdate = employee.Startdate;
            details.State = employee.State;
            details.Telephone = employee.Telephone;
            
            return View(details);
        }

        // GET: Employees/Create
        public IActionResult Create()
        {

            var listrol = (from rol in _context.Roles
                           select new SelectListItem()
                           {
                               Text = rol.Name,
                               Value = rol.Idrole.ToString(),
                           }).ToList();
            ViewBag.roles = listrol;

            var Projects = (from rol in _context.Projects
                            select new SelectListItem()
                            {
                                Text = rol.Name,
                                Value = rol.Idproject.ToString(),
                            }).ToList();
            ViewBag.Projects = Projects;
            return View();
        }

        // POST: Employees/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Idrole,Idproject,Name,LastName,Telephone,Email,Address,Documents,Payrate,Startdate,State")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                _context.Add(employee);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(employee);
        }

        // GET: Employees/Edit/5
        public async Task<IActionResult> Edit(ulong? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees.FindAsync(id);
            if (employee == null)
            {
                return NotFound();
            }
            var listrol = (from rol in _context.Roles
                           select new SelectListItem()
                           {
                               Text = rol.Name,
                               Value = rol.Idrole.ToString(),
                           }).ToList();
            ViewBag.roles = listrol;

            var Projects = (from rol in _context.Projects
                            select new SelectListItem()
                            {
                                Text = rol.Name,
                                Value = rol.Idproject.ToString(),
                            }).ToList();
            ViewBag.Projects = Projects;
            return View(employee);
        }

        // POST: Employees/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ulong id, [Bind("Id,Idrole,Idproject,Name,LastName,Telephone,Email,Address,Documents,Payrate,Startdate,State")] Employee employee)
        {
            if (id != employee.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(employee);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmployeeExists(employee.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(employee);
        }

        // GET: Employees/Delete/5
        public async Task<IActionResult> Delete(ulong? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employee = await _context.Employees
                .FirstOrDefaultAsync(m => m.Id == id);
            if (employee == null)
            {
                return NotFound();
            }

            return View(employee);
        }

        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(ulong id)
        {
            var employee = await _context.Employees.FindAsync(id);
            _context.Employees.Remove(employee);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EmployeeExists(ulong id)
        {
            return _context.Employees.Any(e => e.Id == id);
        }

        public async Task<IActionResult> UploadDocument(ulong idEmployee, IFormFile doc)
        {
            try
            {
                if (doc == null || doc.Length == 0)
                {
                    return Content("File not selected");
                }
                else
                {
                    var employee = await _context.Employees.FindAsync(idEmployee);
                    //Obteniendo el path de la imagen
                    var FilePath = Path.Combine(_webHostEnvironment.WebRootPath, "img/documents/", employee.Id.ToString() + "/");
                    if (!Directory.Exists(FilePath))
                    {
                        Directory.CreateDirectory(FilePath);
                    }
                    var fileExt = System.IO.Path.GetExtension(doc.FileName).Substring(1);
                    var stream = new FileStream(FilePath+ doc.FileName, FileMode.Create);
                    await doc.CopyToAsync(stream);
                    //Agregando docuemtos a la tabla en la BD
                    Document document = new Document();
                    document.IdEmployee = employee.Id;
                    document.Name = doc.FileName;
                    _context.Set<Document>().Add(document);
                    await _context.SaveChangesAsync();
                }
                return RedirectToAction(nameof(Index));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public FileResult download(string id ,string name)
        {
            var fileName = name;
            var FilePath = Path.Combine(_webHostEnvironment.WebRootPath, "img/documents/", id.ToString() + "/"+name);
            byte[] fileBytes = System.IO.File.ReadAllBytes(FilePath);
            return File(fileBytes, "application/txt", fileName);
        }
    }
}
