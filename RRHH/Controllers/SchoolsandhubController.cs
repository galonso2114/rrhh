﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RRHH.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RRHH.Controllers
{
    public class SchoolsandhubController : Controller
    {
        private readonly rrhhContext _context;

        public SchoolsandhubController(rrhhContext context)
        {
            _context = context;
        }

        // GET: Roles
        public async Task<IActionResult> Index()
        {
            return View(await _context.Schoolsandhubs.ToListAsync());
        }

        // GET: Roles/Details/5
        public async Task<IActionResult> Details(ulong? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Schoolsandhubs = await _context.Schoolsandhubs
                .FirstOrDefaultAsync(m => m.Id == id);
            if (Schoolsandhubs == null)
            {
                return NotFound();
            }

            return View(Schoolsandhubs);
        }

        // GET: Roles/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Roles/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SchoolNumbers,SchoolHubs,Date")] Schoolsandhub schoolsandhub)
        {
            if (ModelState.IsValid)
            {
                var str = HttpContext.Session.GetString("User");
                var SessionUsuario = JsonConvert.DeserializeObject<User>(str);
                schoolsandhub.iduser = SessionUsuario.Id;
                _context.Add(schoolsandhub);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(schoolsandhub);
        }

        // GET: Roles/Edit/5
        public async Task<IActionResult> Edit(ulong? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Schoolsandhubs = await _context.Schoolsandhubs.FindAsync(id);
            if (Schoolsandhubs == null)
            {
                return NotFound();
            }
            return View(Schoolsandhubs);
        }

        // POST: Roles/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ulong id, [Bind("Id,SchoolNumbers,SchoolHubs,Date,iduser")] Schoolsandhub schoolsandhub)
        {
            if (id != schoolsandhub.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(schoolsandhub);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SchoolsandhubsExists(schoolsandhub.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(schoolsandhub);
        }

        // GET: Roles/Delete/5
        public async Task<IActionResult> Delete(ulong? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Schoolsandhubs = await _context.Schoolsandhubs
                .FirstOrDefaultAsync(m => m.Id == id);
            if (Schoolsandhubs == null)
            {
                return NotFound();
            }

            return View(Schoolsandhubs);
        }

        // POST: Roles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(ulong id)
        {
            var Schoolsandhubs = await _context.Schoolsandhubs.FindAsync(id);
            _context.Schoolsandhubs.Remove(Schoolsandhubs);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SchoolsandhubsExists(ulong id)
        {
            return _context.Schoolsandhubs.Any(e => e.Id == id);
        }
    }
}
