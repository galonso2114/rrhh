﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Rotativa.AspNetCore;
using RRHH.Filters;
using RRHH.Models;
using RRHH.Models.VM;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RRHH.Controllers
{
    [CustomAuthorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly rrhhContext _context;

        public HomeController(ILogger<HomeController> logger, rrhhContext rrhhContext)
        {
            _logger = logger;
            _context = rrhhContext;

        }
        public IActionResult Index()
        {
            var str = HttpContext.Session.GetString("User");
            var UserSession = JsonConvert.DeserializeObject<User>(str);
            HomeVM home = new HomeVM();
            if (UserSession.Idrole == 1)
            {
                var employee = _context.Employees.ToList();
                foreach (var item in employee)
                {
                    ConsolidatedHoursTable table = new ConsolidatedHoursTable();
                    var totalHoras = _context.Timesheets.Where(x => x.IdEmployee == item.Id).Select(x => (x.End - x.Start)).ToList();
                    var totalSpan = new TimeSpan(totalHoras.Sum(r => r.Value.Ticks));
                    var total = totalSpan.Hours;
                    table.payroll = total;
                    table.Payrate = (decimal)item.Payrate;
                    table.name = item.Name;
                    table.lasname = item.LastName;
                    table.hours = total * table.Payrate;
                    home.consolidatedHours.Add(table);
                }
                home.CountTotalEmployee = employee.Count();
                home.CountEmployeeByProject = employee.Where(x => x.Idproject == UserSession.Idproject).Count();
                home.totalfin = home.consolidatedHours.Sum(x => x.hours);
            }
            else if (UserSession.Idrole == 2)
            {
                var employee = _context.Employees.ToList();
                foreach (var item in employee)
                {
                    ConsolidatedHoursTable table = new ConsolidatedHoursTable();
                    var totalHoras = _context.Timesheets.Where(x => x.IdEmployee == item.Id).Select(x => (x.End - x.Start)).ToList();
                    var totalSpan = new TimeSpan(totalHoras.Sum(r => r.Value.Ticks));
                    var total = totalSpan.Hours;
                    table.payroll = total;
                    table.Payrate = (decimal)item.Payrate;
                    table.name = item.Name;
                    table.lasname = item.LastName;
                    table.hours = total * table.Payrate;
                    home.consolidatedHours.Add(table);
                }
                home.CountTotalEmployee = employee.Count();
                home.CountEmployeeByProject = employee.Where(x => x.Idproject == UserSession.Idproject).Count();
                home.totalfin = home.consolidatedHours.Sum(x => x.hours);
            }
            else if (UserSession.Idrole == 3)
            {
                var employee = _context.Employees.ToList();
                foreach (var item in employee)
                {
                    ConsolidatedHoursTable table = new ConsolidatedHoursTable();
                    var totalHoras = _context.Timesheets.Where(x => x.IdEmployee == item.Id).Select(x => (x.End - x.Start)).ToList();
                    var totalSpan = new TimeSpan(totalHoras.Sum(r => r.Value.Ticks));
                    var total = totalSpan.Hours;
                    table.payroll = total;
                    table.Payrate = (decimal)item.Payrate;
                    table.name = item.Name;
                    table.lasname = item.LastName;
                    table.hours = total * table.Payrate;
                    home.consolidatedHours.Add(table);
                }
                home.CountTotalEmployee = employee.Count();
                home.CountEmployeeByProject = employee.Where(x => x.Idproject == UserSession.Idproject).Count();
                home.totalfin = home.consolidatedHours.Sum(x => x.hours);
            }
            return View(home);
        }
        public IActionResult Privacy()
        {
            return View();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult reports(ulong? id, DateTime? week)
        {
            try
            {
                var Projects = (from rol in _context.Projects
                                select new SelectListItem()
                                {
                                    Text = rol.Name,
                                    Value = rol.Idproject.ToString(),
                                }).ToList();
                ViewBag.Projects = Projects;
                var str = HttpContext.Session.GetString("User");
                var UserSession = JsonConvert.DeserializeObject<User>(str);
                HomeVM home = new HomeVM();
                var idp = id != null ? id : UserSession.Idproject;
                var employee = _context.Employees.Where(x => x.Idproject == idp).ToList();
                DateTime week5 = week != null ? (DateTime)week : DateTime.Now;
                foreach (var item in employee)
                {
                    ConsolidatedHoursTable table = new ConsolidatedHoursTable();
                    var totalHoras = _context.Timesheets.Where(x => x.IdEmployee == item.Id && (x.Date >= week5.Date && x.Date <= week5.Date.AddDays(7))).Select(x => (x.End - x.Start)).ToList();
                    var totallounch = _context.Timesheets.Where(x => x.IdEmployee == item.Id && (x.Date >= week5.Date && x.Date <= week5.Date.AddDays(7))).Sum(x => x.Lunch);
                    var totalSpan = new TimeSpan(totalHoras.Sum(r => r.Value.Ticks));
                    var total = totalSpan.Hours - totallounch;
                    table.payroll = (decimal)total;
                    table.Payrate = (decimal)item.Payrate;
                    table.name = item.Name;
                    table.lasname = item.LastName;
                    table.hours = (decimal)total * table.Payrate;
                    home.consolidatedHours.Add(table);
                }
                home.CountTotalEmployee = employee.Count();
                home.CountEmployeeByProject = employee.Where(x => x.Idproject == UserSession.Idproject).Count();
                home.idpro = (ulong)idp;
                home.week = week != null ? (DateTime)week : DateTime.Now;
                home.totalfin = home.consolidatedHours.Sum(x => x.hours);
                return View(home);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public IActionResult GenerateReport(ulong id, DateTime? week)
        {
            try
            {

                //var Projects = (from rol in _context.Projects
                //                select new SelectListItem()
                //                {
                //                    Text = rol.Name,
                //                    Value = rol.Idproject.ToString(),
                //                }).ToList();
                //ViewBag.Projects = Projects;
                var str = HttpContext.Session.GetString("User");
                var UserSession = JsonConvert.DeserializeObject<User>(str);
                HomeVM home = new HomeVM();
                var employee = _context.Employees.Where(x => x.Idproject == id).ToList();
                DateTime week5 = week != null ? (DateTime)week : DateTime.Now;
                foreach (var item in employee)
                {
                    ConsolidatedHoursTable tablee = new ConsolidatedHoursTable();
                    var totalHoras = _context.Timesheets.Where(x => x.IdEmployee == item.Id && (x.Date >= week5.Date && x.Date <= week5.Date.AddDays(7))).Select(x => (x.End - x.Start)).ToList();
                    var totallounch = _context.Timesheets.Where(x => x.IdEmployee == item.Id && (x.Date >= week5.Date && x.Date <= week5.Date.AddDays(7))).Sum(x => x.Lunch);
                    var totalSpan = new TimeSpan(totalHoras.Sum(r => r.Value.Ticks));
                    var total = totalSpan.Hours - totallounch;
                    tablee.payroll = (decimal)total;
                    tablee.Payrate = (decimal)item.Payrate;
                    tablee.name = item.Name;
                    tablee.lasname = item.LastName;
                    tablee.hours = (decimal)total * tablee.Payrate;
                    home.consolidatedHours.Add(tablee);
                }
                home.totalfin = home.consolidatedHours.Sum(x => x.hours);
                //return new ViewAsPdf("GenerateReport", home)
                //{

                //};
                iTextSharp.text.Document doc = new iTextSharp.text.Document(PageSize.A4, 10f, 10f, 100f, 0f);
                MemoryStream ms = new MemoryStream();
                PdfWriter pdfWriter = PdfWriter.GetInstance(doc, ms);
                doc.AddAuthor("GersonAlonso");
                doc.AddTitle("PRueba");
                doc.Open();
                PdfPTable table = new PdfPTable(5);

                PdfPCell cell = new PdfPCell(new Phrase("Report Hours"));

                cell.Colspan = 5;

                cell.HorizontalAlignment = 1; //0=Left, 1=Centre, 2=Right

                table.AddCell(cell);

                table.AddCell("Name");
                table.AddCell("Lasname");
                table.AddCell("Payrate");
                table.AddCell("Payroll");
                table.AddCell("Total");
                foreach (var item in home.consolidatedHours)
                {
                    table.AddCell(item.name);
                    table.AddCell(item.lasname);
                    table.AddCell(item.Payrate.ToString());
                    table.AddCell(item.payroll.ToString());
                    table.AddCell(item.hours.ToString());
                }
                table.AddCell(" ");
                table.AddCell(" ");
                table.AddCell(" ");
                table.AddCell(" ");
                table.AddCell("Total:" + home.totalfin);
                doc.Add(table);
                //doc.Add(new Phrase("Hola mundo"));
                doc.Close();


                ms.Seek(0, SeekOrigin.Begin);
                return File(ms, "application/pdf", "Grid.pdf");

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
