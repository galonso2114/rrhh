﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RRHH.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RRHH.Controllers
{
    public class ExpensesController : Controller
    {
        private readonly rrhhContext _context;

        public ExpensesController(rrhhContext context)
        {
            _context = context;
        }

        // GET: Roles
        public async Task<IActionResult> Index()
        {
            return View(await _context.Expenses.ToListAsync());
        }

        // GET: Roles/Details/5
        public async Task<IActionResult> Details(ulong? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Expenses = await _context.Expenses
                .FirstOrDefaultAsync(m => m.Id == id);
            if (Expenses == null)
            {
                return NotFound();
            }

            return View(Expenses);
        }

        // GET: Roles/Create
        public IActionResult Create()
        {
            var Projects = (from rol in _context.Projects
                            select new SelectListItem()
                            {
                                Text = rol.Name,
                                Value = rol.Idproject.ToString(),
                            }).ToList();
            Projects.Insert(0, new SelectListItem() { Text = "Select", Value = "0" });
            ViewBag.Projects = Projects;
            return View();
        }

        // POST: Roles/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Date,Description,Idproject,Idemployee,Value")] Expense expense)
        {
            if (ModelState.IsValid)
            {
                expense.State = 1;
                _context.Add(expense);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(expense);
        }

        // GET: Roles/Edit/5
        public async Task<IActionResult> Edit(ulong? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Expenses = await _context.Expenses.FindAsync(id);
            if (Expenses == null)
            {
                return NotFound();
            }
            return View(Expenses);
        }

        // POST: Roles/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ulong id, [Bind("Id,Date,Description,Idproject,Idemployee,Value")] Expense expense)
        {
            if (id != expense.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    expense.Deliverdate = DateTime.Now.Date;
                    _context.Update(expense);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ExpensesExists(expense.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(expense);
        }

        // GET: Roles/Delete/5
        public async Task<IActionResult> Delete(ulong? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var expenses = await _context.Expenses
                .FirstOrDefaultAsync(m => m.Id == id);
            if (expenses == null)
            {
                return NotFound();
            }

            return View(expenses);
        }

        // POST: Roles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(ulong id)
        {
            var Expenses = await _context.Expenses.FindAsync(id);
            _context.Expenses.Remove(Expenses);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ExpensesExists(ulong id)
        {
            return _context.Expenses.Any(e => e.Id == id);
        }
        public async Task<IActionResult> getEmployee(ulong id)
        {
            try
            {
                var employee = await _context.Employees.Where(x => x.Idproject == id).ToListAsync();
                return Json(employee);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public async Task<IActionResult> approvals()
        {
            try
            {
                return View(await _context.Expenses.ToListAsync());
            }
            catch (Exception)
            {

                throw;
            }
        }

        public IActionResult EditApprovals(ulong id)
        {
            try
            {
                var stateapruval = (from rol in _context.Stateapruvals
                                select new SelectListItem()
                                {
                                    Text = rol.Name,
                                    Value = rol.Id.ToString(),
                                }).ToList();
                ViewBag.stateapruval = stateapruval;
                return View();
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditApprovals(ulong id, [Bind("Id,Date,Description,Idproject,Idemployee,Value,State,Payday")] Expense expense)
        {
            if (id != expense.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    expense.Deliverdate = DateTime.Now.Date;
                    _context.Update(expense);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ExpensesExists(expense.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(expense);
        }
    }
}
