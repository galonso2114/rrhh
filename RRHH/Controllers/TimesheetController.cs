﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RRHH.Filters;
using RRHH.Models;
using RRHH.Models.VM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RRHH.Controllers
{
    [CustomAuthorize]
    public class TimesheetController : Controller
    {
        private readonly rrhhContext _context;
        public TimesheetController(rrhhContext context)
        {
            _context = context;
        }
        // GET: TimesheetController
        public async Task<IActionResult> Index(ulong? id, DateTime? week)
        {
            try
            {
                var Projects = (from rol in _context.Projects
                                select new SelectListItem()
                                {
                                    Text = rol.Name,
                                    Value = rol.Idproject.ToString(),
                                }).ToList();
                ViewBag.Projects = Projects;
                var str = HttpContext.Session.GetString("User");
                var SessionUsuario = JsonConvert.DeserializeObject<User>(str);
                if (SessionUsuario.Id == 1)
                {
                    var empleados = await _context.Employees.ToListAsync();
                    return View(empleados);
                }
                else
                {
                    var idp = id != null ? id : SessionUsuario.Idproject;
                    var empleados = await _context.Employees.Where(x => x.Idproject == idp).ToListAsync();
                    //Horas
                    List<EmployeesVM> vMs = new List<EmployeesVM>();
                    DateTime week5 = week != null ? (DateTime)week : DateTime.Now;
                    foreach (var item in empleados)
                    {
                        EmployeesVM vM = new EmployeesVM();
                        var totalHoras = _context.Timesheets.Where(x => x.IdEmployee == item.Id && (x.Date >= week5.Date && x.Date <= week5.Date.AddDays(7))).Select(x => (x.End - x.Start)).ToList();
                        var totallounch = _context.Timesheets.Where(x => x.IdEmployee == item.Id && (x.Date >= week5.Date && x.Date <= week5.Date.AddDays(7))).Sum(x => x.Lunch);
                        var totalSpan = new TimeSpan(totalHoras.Sum(r => r.Value.Ticks));
                        var total = totalSpan.Hours - totallounch;
                        vM.Address = item.Address;
                        vM.Email = item.Email;
                        vM.horas = (decimal)total;
                        vM.Id = item.Id;
                        vM.Idproject = item.Idproject;
                        vM.LastName = item.LastName;
                        vM.Name = item.Name;
                        vM.Payrate = item.Payrate;
                        vM.Startdate = item.Startdate;
                        vM.State = item.State;
                        vM.Telephone = item.Telephone;
                        vMs.Add(vM);
                    }

                    return View(vMs);
                }
            }
            catch (Exception ex)
            {
                var msn = ex.Message;
                throw;
            }
        }

        // GET: TimesheetController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: TimesheetController/Create
        public ActionResult Create(ulong id)
        {
            TimesheetVM vM = new TimesheetVM();
            var employee = _context.Employees.FirstOrDefault(x => x.Id == id);
            var heet = _context.Timesheets.Where(x => x.IdEmployee == id).ToList();
            vM.IdEmployee = (int)employee.Id;
            vM.EmployeeName = employee.Name;
            vM.Idproject = (ulong)employee.Idproject;
            if (heet != null)
            {
                vM.timesheets = heet;
            }
            //var Projects = (from rol in _context.Projects
            //                select new SelectListItem()
            //                {
            //                    Text = rol.Name,
            //                    Value = rol.Idproject.ToString(),
            //                }).ToList();
            //ViewBag.Projects = Projects;
            return View(vM);
        }

        // POST: TimesheetController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdEmployee,Idproject,Date,Start,End,Lunch")] Timesheet timesheet)
        {
            try
            {
                _context.Set<Timesheet>().Add(timesheet);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Create), "Timesheet", new { id = timesheet.IdEmployee });
            }
            catch
            {
                return View();
            }
        }

        // POST: TimesheetController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit([Bind("IdEmployee,Idtimesheet,Idproject,Date,Start,End,Lunch")] Timesheet timesheetn)
        {
            try
            {
                if (timesheetn.Idtimesheet == 0)
                {
                    return NotFound();
                }
                _context.Update(timesheetn);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Create), "Timesheet", new { id = timesheetn.IdEmployee });
            }
            catch
            {
                return View();
            }
        }

        // GET: TimesheetController/Delete/5
        public async Task<IActionResult> Delete(ulong id)
        {
            try
            {
                var employee = await _context.Timesheets.FindAsync(id);
                _context.Timesheets.Remove(employee);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Create), "Timesheet", new { id = employee.IdEmployee });
            }
            catch
            {
                return View();
            }
        }

        // POST: TimesheetController/Delete/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Delete(int id)
        //{
        //    try
        //    {
        //        var employee = await _context.Employees.FindAsync(id);
        //        _context.Employees.Remove(employee);
        //        await _context.SaveChangesAsync();
        //        return RedirectToAction(nameof(Index));

        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
