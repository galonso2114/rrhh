﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RRHH.Filters
{
    public class CustomAuthorize: ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var session = context.HttpContext.Session.GetString("User");
            if (session == null)
            {
                context.Result = new RedirectToRouteResult(
                    new RouteValueDictionary {{ "Controller", "Account" },
                                      { "Action", "Login" } });
            }
        }
    }
}
